import { Logger as PinoLogger } from 'pino';
import {
  Kafka as BaseKafka,
  LogEntry,
  logLevel,
} from 'kafkajs';

import { Logger } from '../../libraries/logger/logger';
import { KafkaConfig } from '../../libraries/interfaces/config';
import { loggerFunction } from './interfaces';

import { Consumer } from './consumer';
import { Producer } from './producer';
import { authTopic } from './constants';

export class Kafka {
  private readonly logger: PinoLogger;

  private readonly config: KafkaConfig;

  private readonly client: BaseKafka;

  private readonly replyClient: BaseKafka;

  private readonly consumer: Consumer;

  private readonly producer: Producer;

  constructor(
    logger: Logger,
    config: KafkaConfig,
  ) {
    this.logger = logger.getLogger('kafka');
    this.config = config;

    this.client = new BaseKafka({
      brokers: this.config.brokers.split(','),
      clientId: this.config.clientId,
      logCreator: (level: logLevel) => this.loggerWrapper(level),
    });

    this.replyClient = new BaseKafka({
      brokers: this.config.brokers.split(','),
      clientId: 'reply-client-id',
      logCreator: (level: logLevel) => this.loggerWrapper(level),
    });
    this.consumer = new Consumer(
      this.client,
      logger,
      this.config.group,
    );
    this.producer = new Producer(this.client, logger);
  }

  public getConsumer(): Consumer {
    return this.consumer;
  }

  public getProducer(): Producer {
    return this.producer;
  }

  public async close(): Promise<void> {
    await this.producer.close();
    await this.consumer.close();
  }

  public async migrate(): Promise<void> {
    const admin = this.client.admin({});

    const topics = await admin.listTopics();
    await admin.createTopics({
      topics: [
        authTopic,
      ]
        .filter((topic) => !topics.includes(topic))
        .map(
          (topic) => ({
            topic,
            numPartitions: this.config.partition,
            replicationFactor: this.config.replication,
          }),
        ),
    });
  }

  private loggerWrapper(level: logLevel): loggerFunction {
    return (entry: LogEntry) => {
      switch (level) {
      case logLevel.DEBUG:
        this.logger.debug(
          {
            timestamp: entry.log.timestamp,
            namespace: entry.namespace,
            label: entry.label,
          },
          entry.log.message,
        );
        break;
      case logLevel.INFO:
        this.logger.info(
          {
            timestamp: entry.log.timestamp,
            namespace: entry.namespace,
            label: entry.label,
          },
          entry.log.message,
        );
        break;
      case logLevel.WARN:
        this.logger.warn(
          {
            timestamp: entry.log.timestamp,
            namespace: entry.namespace,
            label: entry.label,
          },
          entry.log.message,
        );
        break;
      case logLevel.ERROR:
        this.logger.error(
          {
            timestamp: entry.log.timestamp,
            namespace: entry.namespace,
            label: entry.label,
          },
          entry.log.message,
        );
        break;
      default:
        this.logger.trace(
          {
            timestamp: entry.log.timestamp,
            namespace: entry.namespace,
            label: entry.label,
          },
          entry.log.message,
        );
        break;
      }
    };
  }
}
