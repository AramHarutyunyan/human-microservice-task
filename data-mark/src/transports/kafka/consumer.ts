import { Logger as PinoLogger } from 'pino';
import {
  Kafka as BaseKafka,
  Consumer as BaseConsumer,
  EachMessagePayload,
} from 'kafkajs';
import { v4 } from 'uuid';

import { Logger } from '../../libraries/logger/logger';

import { Services } from '../../services';

import { MessageType, ProcessingMessage, VerifiedMessage } from './interfaces';
import { authTopic, processingTopic } from './constants';

export class Consumer {
  private readonly logger: PinoLogger;

  private readonly client: BaseConsumer;

  private readonly broadcast: BaseConsumer;

  private services?: Services

  constructor(
    client: BaseKafka,
    logger: Logger,
    groupId: string,
  ) {
    this.logger = logger.getLogger('kafka-consumers');
    this.client = client.consumer({
      groupId,
    });
    this.broadcast = client.consumer({
      groupId: `${groupId}-${v4()}`,
    });
  }

  public async start(services: Services): Promise<void> {
    this.services = services;
    await this
      .client
      .subscribe({
        topics: [
          processingTopic,
        ],
        fromBeginning: false,
      });
    await this
      .broadcast
      .subscribe({
        topics: [
          authTopic,
        ],
        fromBeginning: false,
      });

    await Promise.all([
      this
        .client
        .run({
          eachMessage: async (payload) => {
            switch (payload.topic) {
            case processingTopic:
              await this.processingTopicHandler(payload);
              break;
            default:
              break;
            }
          },
        }),
      this
        .broadcast
        .run({
          eachMessage: async (payload) => {
            switch (payload.topic) {
            case authTopic:
              if (payload.message.value) {
                const raw = JSON.parse(
                  payload.message.value.toString(),
                );
                const message = raw as ProcessingMessage;

                switch (message.type) {
                case MessageType.verified: {
                  const msg = raw as VerifiedMessage;
                  this.services?.events.emit(
                    `verified-${msg.id}`,
                    msg.error,
                    msg.userId,
                  );
                  break;
                }
                default:
                  break;
                }
              }
              break;
            default:
              break;
            }
          },
        }),
    ]);
  }

  public async close(): Promise<void> {
    return this.client.disconnect();
  }

  private getServices(): Services {
    if (!this.services) {
      throw new Error('Services is not initialized');
    }

    return this.services;
  }

  private async processingTopicHandler(
    payload: EachMessagePayload,
  ): Promise<void> {
    if (payload.message.value) {
      const message = JSON.parse(payload.message.value.toString()) as ProcessingMessage;

      switch (message.type) {
      case MessageType.completeProcessRead:
        await this.services?.mark.saveMarkData(message.data);
        break;
      default:
        break;
      }
    }
  }
}
