import { Logger as PinoLogger } from 'pino';
import {
  Kafka as BaseKafka,
  Producer as BaseProducer,
  Message as BaseMessage,
  ProducerRecord,
} from 'kafkajs';

import { Logger } from '../../libraries/logger/logger';

import { MessageProducer } from '../../services/interfaces';
import { DataMarkMessage, MessageType, VerifyMessage } from './interfaces';
import { authTopic, processingTopic } from './constants';

export class Producer implements MessageProducer {
  private readonly logger: PinoLogger;

  private readonly client: BaseProducer;

  constructor(client: BaseKafka, logger: Logger) {
    this.logger = logger.getLogger('kafka-producer');
    this.client = client.producer({
      allowAutoTopicCreation: false,
      transactionTimeout: 30000,
    });
  }

  public async connect(): Promise<void> {
    return this.client.connect();
  }

  public async close(): Promise<void> {
    return this.client.disconnect();
  }

  public async sendVerify(token: string, id: string): Promise<void> {
    const msg: VerifyMessage = {
      type: MessageType.verify,
      token,
      id,
    };
    const message: BaseMessage = {
      value: JSON.stringify(msg),
    };
    const record: ProducerRecord = {
      topic: authTopic,
      messages: [
        message,
      ],
    };

    await this.client.send(record);
  }

  public async getDataMark(completed: boolean): Promise<void> {
    const msg: DataMarkMessage = {
      type: MessageType.readProcess,
      dataType: completed,
    };
    const message: BaseMessage = {
      value: JSON.stringify(msg),
    };
    const record: ProducerRecord = {
      topic: processingTopic,
      messages: [
        message,
      ],
    };

    await this.client.send(record);
  }
}
