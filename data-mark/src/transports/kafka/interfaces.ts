/* eslint-disable no-unused-vars */
import { LogEntry } from 'kafkajs';

export type loggerFunction = (entry: LogEntry) => void;

export enum MessageType {
  none = '',
  verify = 'verify',
  verified = 'verified',
  readProcess = 'readProcess',
  completeProcessRead = 'completeProcessRead',

}

export interface Message {
  type: MessageType;
}

export interface VerifyMessage extends Message {
  token: string;
  id: string;
}

export interface ProcessingMessage extends Message {
  data: number;
}

export interface VerifiedMessage extends Message {
  status: string;
  error?: string;
  id: string;
  userId?: bigint;
}

export interface DataMarkMessage extends Message {
  dataType: boolean;
}
