/* eslint-disable no-unused-vars */
import { Logger as PinoLogger } from 'pino';
import { Response, NextFunction } from 'express';
import { v4 } from 'uuid';

import { Logger } from '../../../libraries/logger/logger';
import { Request } from '../../../libraries/interfaces/http';

import { MiddlewareHandler } from './interface';
import {
  AuthorizationHeaderRequired,
} from '../../../libraries/errors/authorization-header-required';
import { InvalidJwtToken } from '../../../libraries/errors/invalid-jwt-token';
import { Services } from '../../../services';
import { JwtValidationFailed } from '../../../libraries/errors/jwt-validation-failed';

interface VerifiedResult {
  userId?: bigint;
  error?: string;
}

export class AuthMiddleware implements MiddlewareHandler {
  private readonly logger: PinoLogger;

  private readonly services: Services;

  constructor(services: Services, logger: Logger) {
    this.services = services;
    this.logger = logger.getLogger('auth-middleware');
  }

  public async handler(
    req: Request,
    res: Response,
    next: NextFunction,
  ): Promise<void> {
    const header = req.header('Authorization');

    if (!header) {
      return next(new AuthorizationHeaderRequired());
    }

    const [type, token] = header.split(' ');

    if (type !== 'Bearer' || !token || token === '') {
      return next(new InvalidJwtToken());
    }

    const id = v4();
    await this
      .services
      .producer
      .sendVerify(token, id);

    const result = await new Promise<VerifiedResult>((resolve) => {
      const indexTimeOut = setTimeout(
        () => resolve({
          error: 'Request timeout',
        }),
        30 * 1000,
      );
      this
        .services
        .events
        .once(
          `verified-${id}`,
          (error?: string, userId?: bigint) => {
            resolve({
              error,
              userId,
            });
          },
        );
    });

    if (result.error) {
      return next(new JwtValidationFailed(result.error));
    }

    if (result.userId) {
      req.body.userId = result.userId;
    }

    return next();
  }
}
