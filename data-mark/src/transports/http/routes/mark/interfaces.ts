import { Mark as MarkDomain } from '../../../../domains/mark';

export interface getMarkDataResponse {
  result: {
    data: Array<MarkDomain> | undefined
  };
}
