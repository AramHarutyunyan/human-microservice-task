import { Logger as PinoLogger } from 'pino';
import {
  NextFunction,
  Request as ExpressRequest,
  Response as ExpressResponse,
  Router,
} from 'express';

import { Logger } from '../../../../libraries/logger/logger';

import { Services } from '../../../../services';
import { DataMarkParams } from '../../../../services/mark/interfaces';

import {
  getMarkDataResponse,
} from './interfaces';

export class MarkRoute {
  private readonly logger: PinoLogger;

  private readonly services: Services;

  constructor(services: Services, logger: Logger) {
    this.logger = logger.getLogger('Mark-route');
    this.services = services;
  }

  public init(): Router {
    this.logger.info('Mark route init');
    const router = Router();

    router
      .post(
        '/get-data',
        async (
          ...args
        ) => this.dataMark(...args),
      );

    return router;
  }

  private async dataMark(
    req: ExpressRequest,
    res: ExpressResponse,
    next: NextFunction,
  ): Promise<unknown> {
    try {
      if (
        !req.body.completed
        || req.body.completed === ''
      ) {
        throw new Error('Completed data is not required');
      }

      const params: DataMarkParams = {
        completed: req.body.completed,
        userId: req.body.userId,
      };

      const mark = await this.services.mark.getMarkData(params);
      const response: getMarkDataResponse = {
        result: {
          data: mark,
        },
      };

      return res.json(response);
    } catch (e) {
      return next(e);
    }
  }
}
