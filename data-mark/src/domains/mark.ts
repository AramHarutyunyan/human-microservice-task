export interface Mark {
  id?: bigint;
  processing: number;
  createdAt: Date;
}
