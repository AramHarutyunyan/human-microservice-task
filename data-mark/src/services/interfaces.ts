/* eslint-disable no-unused-vars */
import { EventEmitter } from 'events';

import { Logger } from '../libraries/logger/logger';

import { AppConfig } from '../libraries/interfaces/config';

import { Repositories } from '../repositories';

import {
  DataMarkParams,
} from './mark/interfaces';
import { Mark as MarkDomain } from '../domains/mark';

export interface MessageProducer {
  sendVerify(token: string, id: string): Promise<void>;
  getDataMark(completed: boolean): Promise<void>;
}

export interface Params {
  repositories: Repositories;
  producer: MessageProducer;
  logger: Logger;
  config: AppConfig;
  event: EventEmitter;
}

export interface Mark {
  getMarkData(params: DataMarkParams): Promise<Array<MarkDomain> | undefined>
  saveMarkData(params: number): Promise<void>;
}
