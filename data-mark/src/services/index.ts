import { EventEmitter } from 'events';

import {
  Params,
  Mark,
  MessageProducer,
} from './interfaces';
import { MarkService } from './mark';

export class Services {
  public readonly mark: Mark;

  private readonly params: Params;

  constructor(params: Params) {
    this.params = params;
    this.mark = new MarkService(params);
  }

  public get events(): EventEmitter {
    return this.params.event;
  }

  public get producer(): MessageProducer {
    return this.params.producer;
  }
}
