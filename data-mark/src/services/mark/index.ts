import { Logger as PinoLogger } from 'pino';
import { Repositories } from '../../repositories';
import {
  Mark, MessageProducer, Params,
} from '../interfaces';
import { DataMarkParams } from './interfaces';
import { Mark as MarkDomain } from '../../domains/mark';

export class MarkService implements Mark {
  private readonly logger: PinoLogger;

  private readonly repositories: Repositories;

  private readonly producer: MessageProducer;

  constructor(params: Params) {
    this.logger = params.logger.getLogger('documents-service');
    this.repositories = params.repositories;
    this.producer = params.producer;
  }

  async getMarkData(params: DataMarkParams): Promise<Array<MarkDomain> | undefined> {
    await this.producer.getDataMark(params.completed);
    const getAll = await this.repositories.marks.getAll();
    return getAll;
  }

  async saveMarkData(processing: number): Promise<void> {
    await this.repositories.marks.createMark(
      processing,
    );
  }
}
