/* eslint-disable no-unused-vars */
export interface DataMarkParams {
  completed: boolean;
  userId: string;
}
