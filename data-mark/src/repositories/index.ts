import { Logger } from '../libraries/logger/logger';

import {
  Marks,
} from './interfaces';

import { Postgres } from '../libraries/database/pg';
import { MarkPostgresRepository } from './marks/postgres';

export class Repositories {
  public readonly marks: Marks;

  constructor(
    postgres: Postgres,
    logger: Logger,
  ) {
    const db = postgres.getClient();

    if (!db) {
      throw new Error('pg client is undefined');
    }

    this.marks = new MarkPostgresRepository(
      db,
      logger,
    );
  }
}
