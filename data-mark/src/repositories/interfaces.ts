/* eslint-disable no-unused-vars */

import { Mark as MarkDomain } from '../domains/mark';

export enum OrderType {
  none = 0,
  asc = 1,
  desc = -1,
}

export interface Marks {
  getAll(): Promise<Array<MarkDomain> | undefined>;
  createMark(processingData: number): Promise<boolean | undefined>
}
