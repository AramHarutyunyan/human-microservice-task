/* eslint-disable camelcase */
export interface Mark {
  id: bigint;
  processing: number;
  created_at: string;
}
