import { Logger as PinoLogger } from 'pino';
import { PoolClient } from 'pg';

import { Logger } from '../../libraries/logger/logger';

import { Mark as MarkDomain } from '../../domains/mark';

import { Marks } from '../interfaces';
import { Mark } from './interfaces';

export class MarkPostgresRepository implements Marks {
  private readonly logger: PinoLogger;

  private readonly client: PoolClient;

  constructor(client: PoolClient, logger: Logger) {
    this.client = client;
    this.logger = logger.getLogger('documents-repository');
  }

  public async getAll(): Promise<Array<MarkDomain> | undefined> {
    const result = await this.client
      .query<Mark>(
        `
          SELECT *
          FROM t_marks
        `,
        [],
      );

    if (result.rows.length === 0) {
      return undefined;
    }

    return result.rows.map((data) => MarkPostgresRepository.mapper(data));
  }

  public async createMark(processingData: number): Promise<boolean | undefined> {
    const result = await this.client
      .query<Mark>(
        `
          INSERT INTO t_marks( "processing", "created_at") VALUES ($1, $2);
        `,
        [
          processingData,
          +new Date(),
        ],
      );

    if (result.rows.length === 0) {
      return undefined;
    }

    // return UserPostgresRepository.mapper(result.rows[0].id);
    return true;
  }

  private static mapper(raw: Mark): MarkDomain {
    const result: MarkDomain = {
      id: raw?.id,
      processing: raw.processing,
      createdAt: new Date(parseInt(raw.created_at, 10)),
    };

    return result;
  }
}
