import { PoolClient } from 'pg';

import { BaseMigration } from '../base';
import { Postgres } from '../../pg';

export class CreateAllTable extends BaseMigration {
  private readonly name = '1648756412492_8toyfmc9_create_all_table';

  public async down(): Promise<void> {
    const db = (this.db as Postgres).getClient() as PoolClient;

    try {
      await db.query('BEGIN');
      await db.query('DROP TABLE IF EXISTS t_users;');
      await db.query('COMMIT');
    } catch (e) {
      await db.query('ROLLBACK');
    }
  }

  public async up(): Promise<void> {
    console.log('1648756412492_8toyfmc9_create_all_table');
    const db = (this.db as Postgres).getClient() as PoolClient;

    try {
      await db.query('BEGIN');
      await db.query(
        `
          CREATE TABLE IF NOT EXISTS t_marks
          (
            id         SERIAL
                CONSTRAINT t_mark_pk PRIMARY KEY,
            processing       BIGINT  NOT NULL,
            created_at BIGINT
          );
        `,
      );
      await db.query('COMMIT');
    } catch (e) {
      console.error(e);
      await db.query('ROLLBACK');
    }
  }

  public getName(): string {
    return this.name;
  }
}
