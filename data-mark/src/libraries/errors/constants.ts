/* eslint-disable no-unused-vars */

import { JwtValidationFailed } from './jwt-validation-failed';

export enum ApplicationErrorCodes {
  none = 0,
  invalidJwtToken = 100,
  authorizationHeaderRequired = 101,
  jwtValidationFailed = 102,
}

export enum ServerErrorCodes {
  none = 0,
  notImplementDatabaseInitMethod = 1000,
  notImplementDatabaseGetVersionMethod = 1001,
  notImplementDatabaseGetMigrationsMethod = 1002,
  notImplementDatabaseSetVersionMethod = 1003,
  notImplementDatabaseDeleteVersionMethod = 1004,
  dbClientNotInitialized = 1005,
}
