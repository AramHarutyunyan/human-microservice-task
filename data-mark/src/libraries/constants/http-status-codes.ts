/* eslint-disable no-unused-vars */
export enum HttpStatusCodes {
  none = 0,
  internalServerError = 500,
  badRequest = 400,
  unauthorized = 401,
  ok = 200,
}
