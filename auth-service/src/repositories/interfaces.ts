/* eslint-disable no-unused-vars */

import { User as UserDomain } from '../domains/user';

export enum OrderType {
  none = 0,
  asc = 1,
  desc = -1,
}

export interface Users {
  getByNickname(nickname: string): Promise<UserDomain | undefined>;
  createUser(nickname: string, password: string): Promise<boolean | undefined>;
}
