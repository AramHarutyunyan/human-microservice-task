import { Logger } from '../libraries/logger/logger';

import {
  Users,
} from './interfaces';

import { Postgres } from '../libraries/database/pg';
import { UserPostgresRepository } from './users/postgres';

export class Repositories {
  public readonly users: Users;

  constructor(
    postgres: Postgres,
    logger: Logger,
  ) {
    const db = postgres.getClient();

    if (!db) {
      throw new Error('pg client is undefined');
    }

    this.users = new UserPostgresRepository(
      db,
      logger,
    );
  }
}
