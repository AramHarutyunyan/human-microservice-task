import { Logger as PinoLogger } from 'pino';
import { PoolClient } from 'pg';

import { Logger } from '../../libraries/logger/logger';

import { User as UserDomain } from '../../domains/user';

import { Users } from '../interfaces';
import { User } from './interfaces';

export class UserPostgresRepository implements Users {
  private readonly logger: PinoLogger;

  private readonly client: PoolClient;

  constructor(client: PoolClient, logger: Logger) {
    this.client = client;
    this.logger = logger.getLogger('documents-repository');
  }

  public async getByNickname(nickname: string): Promise<UserDomain | undefined> {
    const result = await this.client
      .query<User>(
        `
          SELECT *
          FROM t_users
          WHERE name = $1;
        `,
        [
          nickname,
        ],
      );

    if (result.rows.length === 0) {
      return undefined;
    }

    return UserPostgresRepository.mapper(result.rows[0]);
  }

  public async createUser(nickname: string, password: string): Promise<boolean | undefined> {
    const result = await this.client
      .query<User>(
        `
          INSERT INTO t_users("name", "nickname", "password", "type") VALUES ($1, $1, $2, $3);
        `,
        [
          nickname,
          password,
          'user',
        ],
      );

    if (result.rows.length === 0) {
      return undefined;
    }

    // return UserPostgresRepository.mapper(result.rows[0].id);
    return true;
  }

  private static mapper(raw: User): UserDomain {
    const result: UserDomain = {
      id: raw?.id,
      name: raw?.name,
      password: raw.password,
      nickName: raw.nickname,
      createdAt: new Date(parseInt(raw.created_at, 10)),
      updatedAt: new Date(parseInt(raw.updated_at, 10)),
    };

    if (raw?.deleted_at && raw?.deleted_at !== '') {
      result.deletedAt = new Date(parseInt(raw.deleted_at, 10));
    }

    return result;
  }
}
