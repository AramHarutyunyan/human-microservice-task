/* eslint-disable camelcase */
export interface User {
  id: bigint;
  name: string;
  nickname: string;
  password: string;
  created_at: string;
  updated_at: string;
  deleted_at?: string;
}
