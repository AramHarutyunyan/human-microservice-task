export interface User {
  id?: bigint;
  name: string;
  nickName: string;
  password: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
}
