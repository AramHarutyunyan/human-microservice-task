import bcrypt from 'bcrypt';

import { AppJwtConfig } from '../interfaces/config';

export class Bcrypt {
    private readonly config: AppJwtConfig;

    private readonly salt = 10;

    constructor(config: AppJwtConfig) {
      this.config = config;
    }

    public async hash(data: string): Promise<string> {
      return bcrypt.hash(data, this.salt);
    }

    public async verify(password: string, hash: string): Promise<boolean> {
      return bcrypt.compare(password, hash);
    }
}
