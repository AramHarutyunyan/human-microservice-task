import { PoolClient } from 'pg';

import { BaseMigration } from '../base';
import { Postgres } from '../../pg';

export class CreateAllTable extends BaseMigration {
  private readonly name = '1648756412472_8toyfmc9_create_all_table';

  public async down(): Promise<void> {
    const db = (this.db as Postgres).getClient() as PoolClient;

    try {
      await db.query('BEGIN');
      await db.query('DROP TABLE IF EXISTS t_users;');
      await db.query('COMMIT');
    } catch (e) {
      await db.query('ROLLBACK');
    }
  }

  public async up(): Promise<void> {
    console.log('1648756412472_8toyfmc9_create_all_table');
    const db = (this.db as Postgres).getClient() as PoolClient;

    try {
      await db.query('BEGIN');
      await db.query(
        `
          CREATE TABLE IF NOT EXISTS t_users
          (
            id         SERIAL
                CONSTRAINT t_users_pk PRIMARY KEY,
            name       VARCHAR(60)  NOT NULL,
            nickname   VARCHAR(20)  NOT NULL,
            password   VARCHAR(100) NOT NULL,
            created_at BIGINT,
            updated_at BIGINT,
            deleted_at BIGINT NULL
          );
        `,
      );
      await db.query(
        `
          CREATE UNIQUE INDEX t_users_unique_nickname
            on public.t_users (nickname);
        `,
      );
      await db.query('COMMIT');
    } catch (e) {
      console.error(e);
      await db.query('ROLLBACK');
    }
  }

  public getName(): string {
    return this.name;
  }
}
