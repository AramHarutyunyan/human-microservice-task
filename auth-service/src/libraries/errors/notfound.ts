import { ApplicationError } from './index';

export class NotFound extends ApplicationError {
  constructor() {
    super('not found');
    this.code = 404;
  }
}
