/* eslint-disable no-unused-vars */
import { LogEntry } from 'kafkajs';

export type loggerFunction = (entry: LogEntry) => void;

export enum MessageType {
  none = '',
  verify = 'verify',
  verified = 'verified',
}

export interface Message {
  type: MessageType;
}

export interface VerifyMessage extends Message {
  token: string;
  id: string;
}

export interface VerifiedMessage extends Message {
  id: string;
  userId: number;
  status: string;
  error?: string;
}
