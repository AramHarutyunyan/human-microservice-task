import { Logger as PinoLogger } from 'pino';
import {
  Kafka as BaseKafka,
  Producer as BaseProducer,
  Message as BaseMessage, ProducerRecord,
} from 'kafkajs';

import { Logger } from '../../libraries/logger/logger';

import { MessageProducer } from '../../services/interfaces';
import { MessageType, VerifiedMessage } from './interfaces';
import { authTopic } from './constants';

export class Producer implements MessageProducer {
  private readonly logger: PinoLogger;

  private readonly client: BaseProducer;

  constructor(client: BaseKafka, logger: Logger) {
    this.logger = logger.getLogger('kafka-producer');
    this.client = client.producer({
      allowAutoTopicCreation: false,
      transactionTimeout: 30000,
    });
  }

  public async connect(): Promise<void> {
    return this.client.connect();
  }

  public async close(): Promise<void> {
    return this.client.disconnect();
  }

  public async sendVerified(status: string,
    msgKey: string,
    userId: number,
    error?: string): Promise<void> {
    const msg: VerifiedMessage = {
      type: MessageType.verified,
      id: msgKey,
      userId,
      status,
      error,
    };
    const message: BaseMessage = {
      value: JSON.stringify(msg),
    };
    const record: ProducerRecord = {
      topic: authTopic,
      messages: [
        message,
      ],
    };

    await this.client.send(record);
  }
}
