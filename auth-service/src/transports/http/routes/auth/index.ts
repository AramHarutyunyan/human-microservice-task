import { Logger as PinoLogger } from 'pino';
import {
  NextFunction,
  Request as ExpressRequest,
  Response as ExpressResponse,
  Router,
} from 'express';

import { Logger } from '../../../../libraries/logger/logger';

import { Services } from '../../../../services';
import { SignInParams } from '../../../../services/auth/interfaces';

import {
  SignInResponse, SigUpResponse,
} from './interfaces';

export class AuthRoute {
  private readonly logger: PinoLogger;

  private readonly services: Services;

  constructor(services: Services, logger: Logger) {
    this.logger = logger.getLogger('collect-route');
    this.services = services;
  }

  public init(): Router {
    this.logger.info('Auth route init');
    const router = Router();

    router
      .post(
        '/signin',
        async (
          ...args
        ) => this.signIn(...args),
      )
      .post(
        '/signup',
        async (
          ...args
        ) => this.signUp(...args),
      );

    return router;
  }

  private async signIn(
    req: ExpressRequest,
    res: ExpressResponse,
    next: NextFunction,
  ): Promise<unknown> {
    try {
      if (
        !req.body.userName
        || req.body.userName === ''
        || !req.body.password
        || req.body.password === ''
      ) {
        throw new Error('Username or password required');
      }

      const params: SignInParams = {
        userName: req.body.userName,
        password: req.body.password,
      };

      const token = await this.services.auth.signIn(params);
      const response: SignInResponse = {
        result: {
          token,
        },
      };

      return res.json(response);
    } catch (e) {
      return next(e);
    }
  }

  private async signUp(
    req: ExpressRequest,
    res: ExpressResponse,
    next: NextFunction,
  ): Promise<unknown> {
    try {
      if (
        !req.body.userName
        || req.body.userName === ''
        || !req.body.password
        || req.body.password === ''
      ) {
        throw new Error('Username or password required');
      }

      const params: SignInParams = {
        userName: req.body.userName,
        password: req.body.password,
      };

      const isSignUp = await this.services.auth.signUp(params);
      const response: SigUpResponse = {
        result: isSignUp,
      };

      return res.json(response);
    } catch (e) {
      return next(e);
    }
  }
}
