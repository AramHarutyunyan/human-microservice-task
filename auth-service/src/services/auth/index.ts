import { Logger as PinoLogger } from 'pino';

import { Repositories } from '../../repositories';

import { Jwt } from '../../libraries/jwt';

import { Bcrypt } from '../../libraries/bcrypt';

import {
  Auth, MessageProducer, Params,
} from '../interfaces';
import { SignInParams, SignUpParams, VerifyParams } from './interfaces';

export class AuthService implements Auth {
  private readonly logger: PinoLogger;

  private readonly repositories: Repositories;

  private readonly producer: MessageProducer;

  private readonly jwt: Jwt;

  private readonly bcrypt: Bcrypt;

  constructor(params: Params) {
    this.logger = params.logger.getLogger('documents-service');
    this.repositories = params.repositories;
    this.jwt = params.jwt;
    this.bcrypt = params.bcrypt;
    this.producer = params.producer;
  }

  async signIn(params: SignInParams): Promise<string> {
    const user = await this.repositories.users.getByNickname(
      params.userName,
    );

    if (!user) {
      throw new Error('Username not exists');
    }

    const verified = await this.bcrypt.verify(params.password, user.password);

    if (!verified) {
      throw new Error('In correct password');
    }

    const token = await this.jwt.sign({ userId: user.id as bigint });

    if (!token) {
      throw new Error('Jwt token not correct generated');
    }

    return token;
  }

  async signUp(params: SignUpParams): Promise<boolean> {
    const user = await this.repositories.users.getByNickname(
      params.userName,
    );
    if (user) {
      throw new Error('Username is exists');
    }

    const hash = await this.bcrypt.hash(params.password);

    await this.repositories.users.createUser(
      params.userName,
      hash,
    );

    if (!hash) {
      throw new Error('you have problem in password Hash');
    }

    return true;
  }

  public async verify(params: VerifyParams): Promise<void> {
    let status = 'success';
    let error = '';
    let data;
    try {
      data = await this.jwt.verify(params.token);
    } catch (e) {
      status = 'fail';
      error = e.message;
    }
    await this.producer.sendVerified(status, params.id, data.data.userId, error);
  }
}
