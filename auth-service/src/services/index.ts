import {
  Params,
  Auth,
} from './interfaces';
import { AuthService } from './auth';

export class Services {
  public readonly auth: Auth;

  constructor(params: Params) {
    this.auth = new AuthService(params);
  }
}
