/* eslint-disable no-unused-vars */
import { Logger } from '../libraries/logger/logger';

import { AppConfig } from '../libraries/interfaces/config';
import { Jwt } from '../libraries/jwt';

import { Repositories } from '../repositories';

import {
  SignInParams,
  SignUpParams,
  VerifyParams,
} from './auth/interfaces';
import { Bcrypt } from '../libraries/bcrypt';

export interface MessageProducer {
  sendVerified(status: string, msgKey: string, userId: number, error?: string): Promise<void>;
}

export interface Params {
  repositories: Repositories;
  producer: MessageProducer;
  jwt: Jwt;
  logger: Logger;
  config: AppConfig;
  bcrypt: Bcrypt;
}

export interface Auth {
  signIn(params: SignInParams): Promise<string>;
  signUp(params: SignUpParams): Promise<boolean>;
  verify(params: VerifyParams): Promise<void>;
}
