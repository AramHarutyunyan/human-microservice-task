import * as path from 'path';

import nconf from 'nconf';
// @ts-ignore
import nconfYaml from 'nconf-yaml';
import FastValidator from 'fastest-validator';
import { Logger as PinoLogger } from 'pino';

import { Config } from '../libraries/interfaces/config';

import { Logger } from '../libraries/logger/logger';

import { configsSchema } from '../libraries/constants/configs';

import { MigrationType } from '../libraries/database/migrations/constants';

import { Repositories } from '../repositories';

import { Params } from '../services/interfaces';
import { Services } from '../services';

import { Server } from './server';
import { Postgres } from '../libraries/database/pg';
import { Kafka } from '../transports/kafka';

export class ProcessingServer extends Server {
  private logger?: PinoLogger;

  private postgres?: Postgres;

  private kafka?: Kafka;

  public async run(rootPath: string): Promise<void> {
    nconf.argv()
      .env({
        lowerCase: true,
        parseValues: true,
        separator: '_',
      })
      .file({
        file: path.join(rootPath, 'configs', 'config.yaml'),
        format: nconfYaml,
      });

    const config: Config = nconf.get();
    const logger = new Logger(config.logger);
    this.logger = logger.getLogger();

    try {
      this.logger.info('server starting');
      const validator = new FastValidator();
      this.logger.info('config checking');
      const configValidationResult = validator.validate(config, configsSchema);

      if (configValidationResult !== true) {
        this.logger.error(configValidationResult);
        return;
      }

      this.logger.info('config is correct');

      this.postgres = new Postgres(config, logger);
      await this.postgres.init();
      await this.postgres.migrate(MigrationType.migrate);

      const repositories = new Repositories(
        this.postgres,
        logger,
      );

      this.kafka = new Kafka(logger, config.kafka);
      await this.kafka.migrate();
      const producer = this.kafka.getProducer();
      await producer.connect();

      const params: Params = {
        repositories,
        producer,
        logger,
        config: config.app,
      };

      const services = new Services(params);

      await this.kafka.getConsumer().start(services);

      this.logger.info('server is started');
    } catch (e) {
      const err = e as Error;
      this.logger.error(err);
    }
  }

  public async stop(): Promise<void> {
    this.postgres?.close();
    await this.kafka?.close();
  }
}
