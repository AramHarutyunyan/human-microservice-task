import { Logger as PinoLogger } from 'pino';
import { PoolClient } from 'pg';

import { Logger } from '../../libraries/logger/logger';

import { ProcessingDomain } from '../../domains/processing';

import { Processing } from '../interfaces';
import { Processing as ProcessingType } from './interfaces';

export class ProcessingPostgresRepository implements Processing {
  private readonly logger: PinoLogger;

  private readonly client: PoolClient;

  constructor(client: PoolClient, logger: Logger) {
    this.client = client;
    this.logger = logger.getLogger('documents-repository');
  }

  public async getAllCompleted(completed: boolean): Promise<Array<ProcessingDomain> | undefined> {
    const result = await this.client
      .query<ProcessingType>(
        `
          SELECT *
          FROM t_processing
          WHERE completed = $1;
        `,
        [
          completed,
        ],
      );

    if (result.rows.length === 0) {
      return undefined;
    }
    // return ProcessingPostgresRepository.mapper(result.rows[0]);
    return result.rows.map<ProcessingDomain>(
      (row) => ProcessingPostgresRepository.mapper(row),
    );
  }

  public async createProcessing(data: Array<ProcessingDomain>): Promise<boolean | undefined> {
    let sql = '';
    data.forEach((processing) => {
      sql = sql !== '' ? `${sql},` : '';
      sql += `('${processing.userId}','${processing.title}','${processing.completed}','${+Date.now()}')`;
    });
    await this.client
      .query<ProcessingType>(
        `
          INSERT INTO t_processing("userid", "title", "completed", "created_at") VALUES ${sql};
        `,
      );

    // return UserPostgresRepository.mapper(result.rows[0].id);
    return true;
  }

  private static mapper(raw: ProcessingType): ProcessingDomain {
    const result: ProcessingDomain = {
      id: raw?.id,
      userId: raw?.userId,
      title: raw.title,
      completed: raw.completed,
      createdAt: Date.now(),
      updatedAt: Date.now(),
    };

    return result;
  }
}
