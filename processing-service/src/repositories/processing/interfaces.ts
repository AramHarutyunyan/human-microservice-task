/* eslint-disable camelcase */
export interface Processing {
  id: bigint;
  userId: number;
  title: string;
  completed: boolean;
  created_at: string;
  updated_at: string;
}
