/* eslint-disable no-unused-vars */

import { ProcessingDomain } from '../domains/processing';

export enum OrderType {
  none = 0,
  asc = 1,
  desc = -1,
}

export interface Processing {
  getAllCompleted(completed: boolean): Promise<Array<ProcessingDomain> | undefined>;
  createProcessing(data: Array<ProcessingDomain>): Promise<boolean | undefined>;
}
