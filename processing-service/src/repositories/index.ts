import { Logger } from '../libraries/logger/logger';

import {
  Processing,
} from './interfaces';

import { Postgres } from '../libraries/database/pg';
import { ProcessingPostgresRepository } from './processing/postgres';

export class Repositories {
  public readonly processing: Processing;

  constructor(
    postgres: Postgres,
    logger: Logger,
  ) {
    const db = postgres.getClient();

    if (!db) {
      throw new Error('pg client is undefined');
    }

    this.processing = new ProcessingPostgresRepository(
      db,
      logger,
    );
  }
}
