import { Logger as PinoLogger } from 'pino';
import {
  Kafka as BaseKafka,
  Consumer as BaseConsumer,
  EachMessagePayload,
} from 'kafkajs';

import { Logger } from '../../libraries/logger/logger';

import { Services } from '../../services';

import { MessageType, ProcessingMessage, ProcessingWriteMessage } from './interfaces';
import { collectorTopic, processingTopic } from './constants';
import { WriteProcessingParams } from '../../services/processing/interfaces';

export class Consumer {
  private readonly logger: PinoLogger;

  private readonly client: BaseConsumer;

  private services?: Services;

  constructor(
    client: BaseKafka,
    logger: Logger,
    groupId: string,
  ) {
    this.logger = logger.getLogger('kafka-consumers');
    this.client = client.consumer({
      groupId,
    });
  }

  public async start(services: Services): Promise<void> {
    this.services = services;
    await this
      .client
      .subscribe({
        topics: [
          processingTopic,
          collectorTopic,
        ],
        fromBeginning: false,
      });

    await this
      .client
      .run({
        eachMessage: async (payload) => {
          switch (payload.topic) {
          case processingTopic:
            await this.readProcessingTopicHandler(payload);
            break;
          case collectorTopic:
            await this.writeProcessingTopicHandler(payload);
            break;
          default:
            break;
          }
        },
      });
  }

  public async close(): Promise<void> {
    return this.client.disconnect();
  }

  private getServices(): Services {
    if (!this.services) {
      throw new Error('Services is not initialized');
    }

    return this.services;
  }

  private async readProcessingTopicHandler(
    payload: EachMessagePayload,
  ): Promise<void> {
    if (payload.message.value) {
      const message = JSON.parse(payload.message.value.toString()) as ProcessingMessage;

      switch (message.type) {
      case MessageType.readProcess:
        await this.services?.processing.readProcessing({ dataType: message.dataType });
        break;
      default:
        break;
      }
    }
  }

  private async writeProcessingTopicHandler(
    payload: EachMessagePayload,
  ): Promise<void> {
    if (payload.message.value) {
      const message = JSON.parse(payload.message.value.toString()) as ProcessingWriteMessage;

      switch (message.type) {
      case MessageType.sendCollector:
        const dataAll:Array<WriteProcessingParams> = [];
        message.data.forEach((data) => {
          dataAll.push({
            userId: data.userId,
            Id: data.id,
            title: data.title,
            completed: data.completed,
          });
        });
        await this.services?.processing.writeProcessing(dataAll);
        break;
      default:
        break;
      }
    }
  }
}
