/* eslint-disable no-unused-vars */
import { LogEntry } from 'kafkajs';

export type loggerFunction = (entry: LogEntry) => void;

export enum MessageType {
  none = '',
  readProcess = 'readProcess',
  writeProcess = 'writeProcess',
  getCollector = 'getCollector',
  sendCollector = 'sendCollector',
  completeReadProcess = 'completeProcessRead',
}

export interface Message {
  type: MessageType;
}

export interface ProcessingMessage extends Message {
  dataType: boolean;
}

export interface ProcessingWriteMessage extends Message {
  data: Array<DataMessage>;
}

export interface DataMessage {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
}

export interface ProcessingSendMessage extends Message {
  status: string;
  error?: string;
  dataType?: string;
}

export interface ProcessingReadMessage extends Message {
  status: string;
  error?: string;
  data: number|undefined;
  dataType?: string;
}
