import {
  Params,
  Processing,
} from './interfaces';
import { ProcessingService } from './processing';

export class Services {
  public readonly processing: Processing;

  constructor(params: Params) {
    this.processing = new ProcessingService(params);
  }
}
