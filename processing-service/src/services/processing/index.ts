import { Logger as PinoLogger } from 'pino';

import { Repositories } from '../../repositories';

import {
  Processing, MessageProducer, Params,
} from '../interfaces';
import { ReadProcessingParams, WriteProcessingParams } from './interfaces';
import { ProcessingDomain } from '../../domains/processing';
// import { MessageType } from '../../transports/kafka/interfaces';

export class ProcessingService implements Processing {
  private readonly logger: PinoLogger;

  private readonly repositories: Repositories;

  private readonly producer: MessageProducer;

  constructor(params: Params) {
    this.logger = params.logger.getLogger('documents-service');
    this.repositories = params.repositories;
    this.producer = params.producer;
  }

  public async readProcessing(params: ReadProcessingParams): Promise<void> {
    let status = 'success';
    let error = '';
    let dataTrue: Array<ProcessingDomain> | undefined;
    let dataFalse: Array<ProcessingDomain> | undefined;
    let allDataCount: number = 0;
    const dataName = 'todos';
    try {
      await this.producer.getDataCollector(status, dataName, error);
      dataTrue = await this.repositories.processing.getAllCompleted(
        params.dataType,
      );
      dataFalse = await this.repositories.processing.getAllCompleted(
        !params.dataType,
      );

      if (!dataTrue || !dataFalse) {
        throw new Error('data is not exists');
      }
      allDataCount = dataTrue?.length + dataFalse?.length;
    } catch (e) {
      status = 'fail';
      error = e.message;
    }
    await this.producer.sendData(status, allDataCount, error);
  }

  public async writeProcessing(params: Array<WriteProcessingParams>): Promise<void> {
    const data = await this.repositories.processing.createProcessing(params);
    if (!data) {
      throw new Error('Data is not exists');
    }
  }
}
