/* eslint-disable no-unused-vars */

export interface ReadProcessingParams {
  dataType: boolean;
}
export interface WriteProcessingParams {
  userId: number;
  Id: number;
  title: string;
  completed: boolean;
}
