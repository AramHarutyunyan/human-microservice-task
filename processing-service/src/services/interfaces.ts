/* eslint-disable no-unused-vars */
import { Logger } from '../libraries/logger/logger';

import { AppConfig } from '../libraries/interfaces/config';

import { Repositories } from '../repositories';

import {
  ReadProcessingParams, WriteProcessingParams,
} from './processing/interfaces';

export interface MessageProducer {
  sendData(status: string, data: number | undefined, error?: string): Promise<void>;
  getDataCollector(
    status: string,
    dataType: string,
    error?: string):Promise<void>;
}

export interface Params {
  repositories: Repositories;
  producer: MessageProducer;
  logger: Logger;
  config: AppConfig;
}

export interface Processing {
  readProcessing(params: ReadProcessingParams): Promise<void>;
  writeProcessing(params: Array<WriteProcessingParams> | undefined): Promise<void>;
}
