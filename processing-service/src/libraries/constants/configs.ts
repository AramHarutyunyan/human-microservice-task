import { ValidationSchema } from 'fastest-validator';
import { ValidationTypes } from './validation-types';

export const configsSchema: ValidationSchema = {
  logger: {
    type: ValidationTypes.object,
    props: {
      levels: {
        type: ValidationTypes.object,
      },
      prettry: {
        type: ValidationTypes.boolean,
      },
      sentry: {
        type: ValidationTypes.object,
        props: {
          dsn: {
            type: ValidationTypes.string,
            optional: true,
            empty: false,
          },
        },
      },
    },
  },
  db: {
    type: ValidationTypes.object,
    props: {
      seed: {
        type: ValidationTypes.object,
        props: {
          version: {
            type: ValidationTypes.string,
            empty: true,
            optional: true,
          },
        },
      },
      pg: {
        type: ValidationTypes.object,
        props: {
          version: {
            type: ValidationTypes.string,
            empty: false,
            optional: true,
          },
          host: {
            type: ValidationTypes.string,
            empty: false,
          },
          port: {
            type: ValidationTypes.number,
          },
          db: {
            type: ValidationTypes.string,
            empty: false,
          },
          user: {
            type: ValidationTypes.string,
            empty: false,
          },
          password: {
            type: ValidationTypes.string,
            empty: false,
          },
        },
      },
    },
  },
  kafka: {
    type: ValidationTypes.object,
    props: {
      clientId: {
        type: ValidationTypes.string,
        empty: false,
      },
      brokers: {
        type: ValidationTypes.string,
        empty: false,
      },
      group: {
        type: ValidationTypes.string,
        empty: false,
      },
      partition: {
        type: ValidationTypes.number,
        positive: true,
      },
      replication: {
        type: ValidationTypes.number,
        positive: true,
      },
    },
  },
};
