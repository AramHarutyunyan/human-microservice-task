/* eslint-disable no-unused-vars */
export enum ApplicationErrorCodes {
  none = 0,
}

export enum ServerErrorCodes {
  none = 0,
  notImplementDatabaseInitMethod = 1000,
  notImplementDatabaseGetVersionMethod = 1001,
  notImplementDatabaseGetMigrationsMethod = 1002,
  notImplementDatabaseSetVersionMethod = 1003,
  notImplementDatabaseDeleteVersionMethod = 1004,
  dbClientNotInitialized = 1005,
}
