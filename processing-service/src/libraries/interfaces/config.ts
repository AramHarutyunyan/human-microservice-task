/* eslint-disable no-unused-vars */

export interface LoggerConfig {
  levels: Record<string, string>;
  prettry: boolean;
  sentry: {
    dsn: string;
  };
}

export interface ListenPortsConfig {
  http: number;
}

export interface ListenConfig {
  ports: ListenPortsConfig;
  host: string
}

export interface AppConfig {
}

export interface SeedConfig {
  version: string;
}

export interface PostgresConfig {
  version: string;
  host: string;
  port: number;
  db: string;
  user: string;
  password: string;
}

export interface DataBaseConfig {
  seed?: SeedConfig;
  pg: PostgresConfig;
}

export interface KafkaConfig {
  clientId: string;
  brokers: string;
  group: string;
  partition: number;
  replication: number;
}

export interface Config {
  app: AppConfig;
  logger: LoggerConfig;
  db: DataBaseConfig;
  kafka: KafkaConfig;
}
