/* eslint-disable no-unused-vars */
import { Direction } from './constants';
import { BaseMigration } from './base';

export interface MigrationData {
  instance: BaseMigration;
  version: string;
  direction: Direction;
}

export interface MigrationItem {
  id: number;
  version: string;
  createdAt: Date;
}
