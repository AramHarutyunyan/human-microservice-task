import path from 'path';
import fs from 'fs';

import { compile } from 'handlebars';
import _ from 'lodash';

import {
  DataBaseType,
  sqliteTemplate,
  symbols,
  typeValueMap,
  valueTypeMap,
} from './constants';

export class Generator {
  private readonly type: DataBaseType;

  private readonly name: string;

  private readonly randomPrefixLength = 8;

  constructor(type: string, name: string) {
    this.name = name;

    if (!typeValueMap[type]) {
      throw new Error('invalid migration type');
    }

    this.type = typeValueMap[type];
  }

  public async generate(): Promise<void> {
    const name = this.fileName();
    const filePath = this.path();

    const render = compile(sqliteTemplate);

    return fs.promises
      .writeFile(
        path.join(filePath, `${name}.ts`),
        render({
          className: _.upperFirst(
            _.camelCase(this.name),
          ),
          name,
        }),
      );
  }

  private fileName(): string {
    const prefix = new Array(this.randomPrefixLength)
      .fill(1)
      .map(() => symbols[Math.floor(Math.random() * 36)])
      .join('');

    return `${Date.now()}_${prefix}_${this.name}`;
  }

  private path(): string {
    return path.resolve(
      path.join(
        'src',
        'libraries',
        'database',
        'migrations',
        valueTypeMap[this.type],
      ),
    );
  }
}
