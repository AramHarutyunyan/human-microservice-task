import { PoolClient } from 'pg';

import { BaseMigration } from '../base';
import { Postgres } from '../../pg';

export class CreateAllTable extends BaseMigration {
  private readonly name = '1648756412473_8toyfmc9_create_all_table';

  public async down(): Promise<void> {
    const db = (this.db as Postgres).getClient() as PoolClient;

    try {
      await db.query('BEGIN');
      await db.query('DROP TABLE IF EXISTS processing;');
      await db.query('COMMIT');
    } catch (e) {
      await db.query('ROLLBACK');
    }
  }

  public async up(): Promise<void> {
    console.log('1648756412473_8toyfmc9_create_all_table');
    const db = (this.db as Postgres).getClient() as PoolClient;

    try {
      await db.query('BEGIN');
      await db.query(
        `
          CREATE TABLE IF NOT EXISTS t_processing
          (
            id         SERIAL
                CONSTRAINT t_processing_pk PRIMARY KEY,
            userId       BIGINT  NOT NULL,
            title VARCHAR(360)  NOT NULL,
            completed VARCHAR(30) NOT NULL,
            created_at BIGINT,
            updated_at BIGINT
          );
        `,
      );
      // await db.query(
      //   `
      //     CREATE UNIQUE INDEX t_processing_unique_nickname
      //       on public.t_processing (nickname);
      //   `,
      // );
      await db.query('COMMIT');
    } catch (e) {
      console.error(e);
      await db.query('ROLLBACK');
    }
  }

  public getName(): string {
    return this.name;
  }
}
