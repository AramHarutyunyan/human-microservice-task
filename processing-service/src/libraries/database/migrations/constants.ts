/* eslint-disable no-unused-vars */
export enum DataBaseType {
  none = 0,
  sqlite = 1,
  seed = 2,
  postgres = 3,
}

export enum MigrationType {
  none = 0,
  migrate = 1,
  seed = 2,
  postgres = 3,
}

export const typeValueMap: Record<string, DataBaseType> = {
  sqlite: DataBaseType.sqlite,
  seed: DataBaseType.seed,
  postgres: DataBaseType.postgres,
};

export const valueTypeMap: Record<DataBaseType, string> = {
  [DataBaseType.sqlite]: 'sqlite',
  [DataBaseType.seed]: 'seed',
  [DataBaseType.postgres]: 'postgres',
  [DataBaseType.none]: '',
};

export enum Direction {
  down = 'down',
  up = 'up',
}

export const symbols = '0123456789abcdefghijklmnopqrstuvwxyz';

export const sqliteTemplate = `import { Database } from 'better-sqlite3';

import { BaseMigration } from '../base';
import { Sqlite } from '../../sqlite';

export class {{className}} extends BaseMigration {
  private readonly name = '{{name}}';

  public async down(): Promise<void> {
    const db = (this.db as Sqlite).getClient() as Database;

    db.exec('');
  }

  public async up(): Promise<void> {
    const db = (this.db as Sqlite).getClient() as Database;
    db.exec(
      \`
      \`,
    );
  }

  public getName(): string {
    return this.name;
  }
}
`;

export const postgresTemplate = `import { PoolClient } from 'pg';

import { BaseMigration } from '../base';
import { Postgres } from '../../pg';

export class {{className}} extends BaseMigration {
  private readonly name = '{{name}}';

  public async down(): Promise<void> {
    const db = (this.db as Postgres).getClient() as PoolClient;

    await db.query('');
  }

  public async up(): Promise<void> {
    const db = (this.db as Postgres).getClient() as PoolClient;
    await db.query(
      \`
      \`,
    );
  }

  public getName(): string {
    return this.name;
  }
}
`;

export const templateMap: Record<DataBaseType, string> = {
  [DataBaseType.sqlite]: sqliteTemplate,
  [DataBaseType.seed]: sqliteTemplate,
  [DataBaseType.postgres]: postgresTemplate,
  [DataBaseType.none]: '',
};
