export interface ProcessingDomain {
  id?: bigint;
  userId: number;
  title: string;
  completed?: boolean;
  createdAt?: number;
  updatedAt?: number;
}

export interface DataCollectorDomain {
  dataType: string;
}
