/* eslint-disable no-console */
import * as path from 'path';

import { ProcessingServer } from '../src/apps/processing-server';

async function main() {
  const rootPath = path.join(__dirname, '..');
  console.log(rootPath);

  const server = new ProcessingServer();

  process.once('SIGTERM', async () => {
    console.log('SIGTERM received');

    return server.stop();
  });
  process.once('SIGINT', async () => {
    console.log('SIGINT received');

    return server.stop();
  });

  return server.run(rootPath);
}

main()
  .catch(console.error);
