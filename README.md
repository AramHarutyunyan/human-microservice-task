About installation,
you must copy in every folder /configs/initial.yaml to /configs/config.yaml

kafka data send

    service name data-collector

    topic name "collector"
    {
        "type" : "getCollector",
        "dataType" : "dataType123"
    },

    service name auth-service

    topic name "auth"
    {
        "type": "verify",
        "token" : "YourTokenHere"
    }

    service name processing-service

    topic name "processing"
    {
        "type": "readProcess",
        "dataType" : "true"
    }

HTTP requests

    HTTP requests signin
    request POST 127.0.0.1:3001/api/auth/signin
    {
    "userName": "aramwebmail1@gmail.com",
    "password": "test"
    }

    HTTP requests signUp
    request POST 127.0.0.1:3001/api/auth/signup
    {
    "userName": "aramwebmail1@gmail.com",
    "password": "test"
    }

    HTTP requests data-mark
    request POST 127.0.0.1:4001/mark/get-data
    Headers-> Authorization: Bearer Your.JWT.Here
    {
        "completed": "true"
    }
