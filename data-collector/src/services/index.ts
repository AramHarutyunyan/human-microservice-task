import {
  Params,
} from './interfaces';
import { CollectService } from './collect';

export class Services {
  public readonly collect: CollectService;

  constructor(params: Params) {
    this.collect = new CollectService(params);
  }
}
