/* eslint-disable no-unused-vars */
import { Logger } from '../libraries/logger/logger';

import { AppConfig } from '../libraries/interfaces/config';

import {
  isVerifyParams,
  collectorParams, kafkaVerifyToken,
} from './collect/interfaces';
import { HttpRequest } from '../libraries/https';

export interface MessageProducer {
  sendCollectorData(status: string, data: any, error?: string): Promise<void>;
}

export interface Params {
  producer: MessageProducer;
  https: HttpRequest;
  logger: Logger;
  config: AppConfig;
}

export interface Collect {
  getData(params: collectorParams): Promise<void>;
}
