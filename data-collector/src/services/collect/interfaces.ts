/* eslint-disable no-unused-vars */
export interface collectorParams {
  dataType: string;
}

export interface isVerifyParams {
  dataType: string;
  token: string;
}

export interface kafkaVerifyToken {
  token: string;
}
