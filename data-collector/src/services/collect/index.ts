import { Logger as PinoLogger } from 'pino';

import { HttpRequest } from '../../libraries/https';

import {
  Collect, MessageProducer, Params,
} from '../interfaces';
import { collectorParams } from './interfaces';

export class CollectService implements Collect {
  private readonly logger: PinoLogger;

  private readonly producer: MessageProducer;

  private readonly https: HttpRequest;

  constructor(params: Params) {
    this.logger = params.logger.getLogger('documents-service');
    this.producer = params.producer;
    this.https = params.https;
  }

  async getData(params: collectorParams): Promise<void> {
    console.log(params);
    let status = 'success';
    let error = '';
    let data: unknown = [];
    try {
      data = await this.https.getCollectorTodos();
    } catch (e) {
      status = 'fail';
      error = e.message;
    }
    await this.producer.sendCollectorData(status, data, error);
  }
}
