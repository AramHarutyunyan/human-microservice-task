import { Logger as PinoLogger } from 'pino';
import {
  Kafka as BaseKafka,
  Consumer as BaseConsumer,
  EachMessagePayload,
} from 'kafkajs';

import { Logger } from '../../libraries/logger/logger';

import { Services } from '../../services';

import { MessageType, VerifyMessage } from './interfaces';
import { collectorTopic } from './constants';

export class Consumer {
  private readonly logger: PinoLogger;

  private readonly client: BaseConsumer;

  private services?: Services

  constructor(
    client: BaseKafka,
    logger: Logger,
    groupId: string,
  ) {
    this.logger = logger.getLogger('kafka-consumers');
    this.client = client.consumer({
      groupId,
    });
  }

  public async start(services: Services): Promise<void> {
    this.services = services;
    await this
      .client
      .subscribe({
        topics: [
          collectorTopic,
        ],
        fromBeginning: false,
      });

    await this
      .client
      .run({
        eachMessage: async (payload) => {
          switch (payload.topic) {
          case collectorTopic:
            await this.collectorTopicHandler(payload);
            break;
          default:
            break;
          }
        },
      });
  }

  public async close(): Promise<void> {
    return this.client.disconnect();
  }

  private getServices(): Services {
    if (!this.services) {
      throw new Error('Services is not initialized');
    }

    return this.services;
  }

  private async collectorTopicHandler(
    payload: EachMessagePayload,
  ): Promise<void> {
    if (payload.message.value) {
      const message = JSON.parse(payload.message.value.toString()) as VerifyMessage;

      switch (message.type) {
      case MessageType.getCollector:
        await this.services?.collect.getData({ dataType: message.dataType });
        break;
      default:
        break;
      }
    }
  }
}
