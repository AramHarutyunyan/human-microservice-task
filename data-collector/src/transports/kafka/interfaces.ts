/* eslint-disable no-unused-vars */
import { LogEntry } from 'kafkajs';

export type loggerFunction = (entry: LogEntry) => void;

export enum MessageType {
  none = '',
  getCollector = 'getCollector',
  sendCollector = 'sendCollector',
  verify = 'verify',
  verified = 'verified',
}

export interface Message {
  type: MessageType;
}

export interface VerifyMessage extends Message {
  dataType: string;
  token: string;
}

export interface CollectorMessage extends Message {
  data: any;
  status: string;
  error?: string;
}

export interface VerifiedMessage extends Message {
  token: any;
  status: string;
  error?: string;
}
