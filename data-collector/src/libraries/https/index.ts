import axios from 'axios';

export class HttpRequest {
  config;

  constructor(config) {
    this.config = config;
  }

  async getCollectorTodos() {
    const data = await new Promise((resolve, reject) => {
      axios.get(this.config, {}).then((res) => {
        resolve(res.data);
      })
        .catch((error) => {
          reject(error);
        });
    });

    return data;
  }
}
