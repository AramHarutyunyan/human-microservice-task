/* eslint-disable no-unused-vars */
export enum ApplicationErrorCodes {
  none = 0,
}

export enum ServerErrorCodes {
  none = 0,
  dbClientNotInitialized = 1005,
}
