/* eslint-disable no-unused-vars */
import { Algorithm } from 'jsonwebtoken';

export interface LoggerConfig {
  levels: Record<string, string>;
  prettry: boolean;
  sentry: {
    dsn: string;
  };
}

export interface CollectorConfig {
  address: string
}

export interface AppJwtConfig {
  secret: string;
  expire: number;
  algorithm: Algorithm;
}

export interface AppConfig {
  jwt: AppJwtConfig;
}

export interface SeedConfig {
  version: string;
}

export interface PostgresConfig {
  version: string;
  host: string;
  port: number;
  db: string;
  user: string;
  password: string;
}

export interface DataBaseConfig {
  seed?: SeedConfig;
  pg: PostgresConfig;
}

export interface KafkaConfig {
  clientId: string;
  brokers: string;
  group: string;
  partition: number;
  replication: number;
}

export interface Config {
  app: AppConfig;
  logger: LoggerConfig;
  collector: CollectorConfig;
  db: DataBaseConfig;
  kafka: KafkaConfig;
}
