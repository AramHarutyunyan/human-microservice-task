import { ValidationSchema } from 'fastest-validator';
import { ValidationTypes } from './validation-types';

export const configsSchema: ValidationSchema = {
  app: {
    type: ValidationTypes.object,
    props: {
      jwt: {
        type: ValidationTypes.object,
        props: {
          secret: {
            type: ValidationTypes.string,
            empty: false,
          },
          expire: {
            type: ValidationTypes.number,
            positive: true,
          },
          algorithm: {
            type: ValidationTypes.string,
            empty: false,
          },
        },
      },
    },
  },
  logger: {
    type: ValidationTypes.object,
    props: {
      levels: {
        type: ValidationTypes.object,
      },
      prettry: {
        type: ValidationTypes.boolean,
      },
      sentry: {
        type: ValidationTypes.object,
        props: {
          dsn: {
            type: ValidationTypes.string,
            optional: true,
            empty: false,
          },
        },
      },
    },
  },
  kafka: {
    type: ValidationTypes.object,
    props: {
      clientId: {
        type: ValidationTypes.string,
        empty: false,
      },
      brokers: {
        type: ValidationTypes.string,
        empty: false,
      },
      group: {
        type: ValidationTypes.string,
        empty: false,
      },
      partition: {
        type: ValidationTypes.number,
        positive: true,
      },
      replication: {
        type: ValidationTypes.number,
        positive: true,
      },
    },
  },
  collector: {
    type: ValidationTypes.object,
    props: {
      address: {
        type: ValidationTypes.string,
        empty: false,
      },
    },
  },
};
